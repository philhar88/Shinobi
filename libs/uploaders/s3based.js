var fs = require('fs');
module.exports = function(s,config,lang){
    //Wasabi Hot Cloud Storage
    var beforeAccountSaveForWasabiHotCloudStorage = function(d){
        //d = save event
        d.formDetails.whcs_use_global=d.d.whcs_use_global
        d.formDetails.use_whcs=d.d.use_whcs
    }
    var cloudDiskUseStartupForWasabiHotCloudStorage = function(group,userDetails){
        group.cloudDiskUse = group.cloudDiskUse || {}
        group.cloudDiskUse['whcs'] = group.cloudDiskUse['whcs'] || {}
        group.cloudDiskUse['whcs'].name = 'Wasabi Hot Cloud Storage'
        group.cloudDiskUse['whcs'].sizeLimitCheck = (userDetails.use_whcs_size_limit === '1')
        if(!userDetails.whcs_size_limit || userDetails.whcs_size_limit === ''){
            group.cloudDiskUse['whcs'].sizeLimit = 10000
        }else{
            group.cloudDiskUse['whcs'].sizeLimit = parseFloat(userDetails.whcs_size_limit)
        }
    }
    var loadWasabiHotCloudStorageForUser = function(e){
        // e = user
        var userDetails = JSON.parse(e.details)
        if(userDetails.whcs_use_global === '1' && config.cloudUploaders && config.cloudUploaders.WasabiHotCloudStorage){
            // {
            //     whcs_accessKeyId: "",
            //     whcs_secretAccessKey: "",
            //     whcs_region: "",
            //     whcs_bucket: "",
            //     whcs_dir: "",
            // }
            userDetails = Object.assign(userDetails,config.cloudUploaders.WasabiHotCloudStorage)
        }
        //Wasabi Hot Cloud Storage
        if(!s.group[e.ke].whcs &&
           userDetails.whcs !== '0' &&
           userDetails.whcs_accessKeyId !== ''&&
           userDetails.whcs_secretAccessKey &&
           userDetails.whcs_secretAccessKey !== ''&&
           userDetails.whcs_bucket !== ''
          ){
            if(!userDetails.whcs_dir || userDetails.whcs_dir === '/'){
                userDetails.whcs_dir = ''
            }
            if(!userDetails.whcs_delivery_hostname){
                userDetails.whcs_delivery_hostname = ''
            }
            if(userDetails.whcs_dir !== ''){
                userDetails.whcs_dir = s.checkCorrectPathEnding(userDetails.whcs_dir)
            }
            if(userDetails.use_whcs_endpoint_select && userDetails.use_whcs_endpoint_select !== ''){
                userDetails.whcs_endpoint = userDetails.use_whcs_endpoint_select
            }
            if(!userDetails.whcs_endpoint || userDetails.whcs_endpoint === ''){
                userDetails.whcs_endpoint = 's3.wasabisys.com'
            }
            var whcs_region = null
            if(userDetails.whcs_region && userDetails.whcs_region !== ''){
                whcs_region = userDetails.whcs_region
            }
            var whcs_storageClass = userDetails.whcs_storageClass
            if(userDetails.whcs_storageClass && userDetails.whcs_storageClass !== ''){
                whcs_storageClass = 'STANDARD'
            }
            var endpointSplit = userDetails.whcs_endpoint.split('.')
            if(endpointSplit.length > 2){
                endpointSplit.shift()
            }
            var locationUrl = endpointSplit.join('.')
            var AWS = new require("aws-sdk")
            s.group[e.ke].whcs = AWS
            var wasabiEndpoint = new AWS.Endpoint(userDetails.whcs_endpoint)
            s.group[e.ke].whcs.config = new s.group[e.ke].whcs.Config({
                endpoint: wasabiEndpoint,
                accessKeyId: userDetails.whcs_accessKeyId,
                secretAccessKey: userDetails.whcs_secretAccessKey,
                region: whcs_region,
                logger: console
            })
            s.group[e.ke].whcs = new s.group[e.ke].whcs.S3();
        }
    }
    var unloadWasabiHotCloudStorageForUser = function(user){
        s.group[user.ke].whcs = null
    }
    var deleteVideoFromWasabiHotCloudStorage = function(e,video,callback){
        // e = user
        try{
            var videoDetails = JSON.parse(video.details)
        }catch(err){
            var videoDetails = video.details
        }
        if(!videoDetails.location){
            videoDetails.location = video.href.split(locationUrl)[1]
        }
        s.group[e.ke].whcs.deleteObject({
            Bucket: s.group[e.ke].init.whcs_bucket,
            Key: videoDetails.location,
        }, function(err, data) {
            if (err) console.log(err);
            callback()
        });
    }
    var uploadVideoToWasabiHotCloudStorage = function(e,k){
        //e = video object
        //k = temporary values
        if(!k)k={};
        //cloud saver - Wasabi Hot Cloud Storage
        if(s.group[e.ke].whcs && s.group[e.ke].init.use_whcs !== '0' && s.group[e.ke].init.whcs_save === '1'){
            var ext = k.filename.split('.')
            ext = ext[ext.length - 1]
            var fileStream = fs.createReadStream(k.dir+k.filename);
            fileStream.on('error', function (err) {
                console.error(err)
            })
            var bucketName = s.group[e.ke].init.whcs_bucket
            var saveLocation = s.group[e.ke].init.whcs_dir+e.ke+'/'+e.mid+'/'+k.filename
            var storageClass = s.group[e.ke].init.whcs_storageClass
            // gcp does not support multipart.  Set queueSize to 1 and a big enough partSize
            var options = s.group[e.ke].whcs.endpoint.href.includes("https://storage.googleapis.com") ? {
                queueSize: 1,
                partSize: 300 * 1024 * 1024
            } : {}
            s.group[e.ke].whcs.upload({
                Bucket: bucketName,
                Key: saveLocation,
                Body:fileStream,
                ACL:'private',
                CacheControl:'max-age=31536000, public',
                StorageClass: storageClass,
                ContentType:'video/'+ext
            },options,function(err,data){
                if(err){
                    console.error(err)
                    s.userLog(e,{type:lang['Wasabi Hot Cloud Storage Upload Error'],msg:err})
                }
                if(s.group[e.ke].init.whcs_log === '1' && data && data.Location){
                    var cloudLink = data.Location
                    cloudLink = fixCloudianUrl(e,cloudLink)
                    s.knexQuery({
                        action: "insert",
                        table: "Cloud Videos",
                        insert: {
                            mid: e.mid,
                            ke: e.ke,
                            time: k.startTime,
                            status: 1,
                            details: s.s({
                                type : 'whcs',
                                location : saveLocation
                            }),
                            size: k.filesize,
                            end: k.endTime,
                            href: cloudLink
                        }
                    })
                    s.setCloudDiskUsedForGroup(e.ke,{
                        amount : k.filesizeMB,
                        storageType : 'whcs'
                    })
                    s.purgeCloudDiskForGroup(e,'whcs')
                }
            })
        }
    }
    var onInsertTimelapseFrame = function(monitorObject,queryInfo,filePath){
        var e = monitorObject
        if(s.group[e.ke].whcs && s.group[e.ke].init.use_whcs !== '0' && s.group[e.ke].init.whcs_save === '1'){
            var fileStream = fs.createReadStream(filePath)
            fileStream.on('error', function (err) {
                console.error(err)
            })
            var saveLocation = s.group[e.ke].init.whcs_dir + e.ke + '/' + e.mid + '_timelapse/' + queryInfo.filename
            s.group[e.ke].whcs.upload({
                Bucket: s.group[e.ke].init.whcs_bucket,
                Key: saveLocation,
                Body: fileStream,
                ACL:'private',
                CacheControl:'max-age=31536000, public',
                StorageClass: whcs_storageClass,
                ContentType:'image/jpeg'
            },function(err,data){
                if(err){
                    s.userLog(e,{type:lang['Wasabi Hot Cloud Storage Upload Error'],msg:err})
                }
                if(s.group[e.ke].init.whcs_log === '1' && data && data.Location){
                    s.knexQuery({
                        action: "insert",
                        table: "Cloud Timelapse Frames",
                        insert: {
                            mid: queryInfo.mid,
                            ke: queryInfo.ke,
                            time: queryInfo.time,
                            details: s.s({
                                type : 'whcs',
                                location : saveLocation
                            }),
                            size: queryInfo.size,
                            href: data.Location
                        }
                    })
                    s.setCloudDiskUsedForGroup(e.ke,{
                        amount : s.kilobyteToMegabyte(queryInfo.size),
                        storageType : 'whcs'
                    },'timelapseFrames')
                    s.purgeCloudDiskForGroup(e,'whcs','timelapseFrames')
                }
            })
        }
    }
    var onDeleteTimelapseFrameFromCloud = function(e,frame,callback){
        // e = user
        try{
            var frameDetails = JSON.parse(frame.details)
        }catch(err){
            var frameDetails = frame.details
        }
        if(frameDetails.type !== 'whcs'){
            return
        }
        if(!frameDetails.location){
            frameDetails.location = frame.href.split(locationUrl)[1]
        }
        s.group[e.ke].whcs.deleteObject({
            Bucket: s.group[e.ke].init.whcs_bucket,
            Key: frameDetails.location,
        }, function(err, data) {
            if (err) console.log(err);
            callback()
        });
    }
    var fixCloudianUrl = function(e,cloudLink){
        var deliveryHostname = s.group[e.ke].init.whcs_delivery_hostname
        var endpoint = s.group[e.ke].init.whcs_endpoint
        var bucket = s.group[e.ke].init.whcs_bucket
        url = new URL(cloudLink)
        if (deliveryHostname === '') {
          url.hostname = bucket + '.' + (new URL(endpoint)).hostname
        } else {
          url.hostname = deliveryHostname
        }
        return url.toString()
    }
    //wasabi
    s.addCloudUploader({
        name: 'whcs',
        loadGroupAppExtender: loadWasabiHotCloudStorageForUser,
        unloadGroupAppExtender: unloadWasabiHotCloudStorageForUser,
        insertCompletedVideoExtender: uploadVideoToWasabiHotCloudStorage,
        deleteVideoFromCloudExtensions: deleteVideoFromWasabiHotCloudStorage,
        cloudDiskUseStartupExtensions: cloudDiskUseStartupForWasabiHotCloudStorage,
        beforeAccountSave: beforeAccountSaveForWasabiHotCloudStorage,
        onAccountSave: cloudDiskUseStartupForWasabiHotCloudStorage,
        onInsertTimelapseFrame: onInsertTimelapseFrame,
        onDeleteTimelapseFrameFromCloud: onDeleteTimelapseFrameFromCloud
    })
    return {
       "evaluation": "details.use_whcs !== '0'",
       "name": lang["S3-Based Network Storage"],
       "color": "forestgreen",
       "info": [{
		"name": "detail=whcs_save",
		"selector": "autosave_whcs",
		"field": lang.Autosave,
		"description": "",
		"default": lang.No,
		"example": "",
		"fieldType": "select",
		"possible": [{
				"name": lang.No,
				"value": "0"
			},
			{
				"name": lang.Yes,
				"value": "1"
			}
		]
	},
	{
		"hidden": true,
		"name": "detail=use_whcs_endpoint_select",
		"selector": "h_whcs_endpoint",
		"field": lang.Endpoint,
		"description": "",
		"default": "",
		"example": "",
		"fieldType": "select",
		"possible": [{
				"name": "Custom Endpoint",
				"value": ""
			},
			{
				"name": lang['Wasabi Hot Cloud Storage'],
				"value": "s3.wasabisys.com"
			}
		]
	},
	{
		"hidden": true,
		"field": lang['Endpoint Address'],
		"name": "detail=whcs_endpoint",
		"placeholder": "s3.wasabisys.com",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"form-group-class-pre-layer": "h_whcs_endpoint_input h_whcs_endpoint_",
		"description": "",
		"default": "",
		"example": "",
		"possible": ""
	},
	{
		"hidden": true,
		"field": lang.Bucket,
		"name": "detail=whcs_bucket",
		"placeholder": "Example : slippery-seal",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": ""
	},
	{
		"hidden": true,
		"field": lang.aws_accessKeyId,
		"name": "detail=whcs_accessKeyId",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": ""
	},
	{
		"hidden": true,
		"name": "detail=whcs_secretAccessKey",
		"fieldType": "password",
		"placeholder": "",
		"field": lang.aws_secretAccessKey,
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": ""
	},
	{
		"hidden": true,
		"name": "detail=whcs_region",
		"field": lang.Region,
		"fieldType": "select",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": [{
				"name": lang['No Region'],
				"value": ""
			},
			{
				"name": "US East (Ohio)",
				"value": "us-east-2"
			},
			{
				"name": "US West (N. California)",
				"value": "us-west-1"
			},
			{
				"name": "US West (Oregon)",
				"value": "us-west-2"
			},
			{
				"name": "Canada (Central)",
				"value": "ca-central-1"
			},
			{
				"name": "EU (Ireland)",
				"value": "eu-west-1"
			},
			{
				"name": "EU (Frankfurt)",
				"value": "eu-central-1"
			},
			{
				"name": "EU (London)",
				"value": "eu-west-2"
			},
			{
				"name": "EU (Paris)",
				"value": "eu-west-3"
			},
			{
				"name": "EU (Stockholm)",
				"value": "eu-north-1"
			},
			{
				"name": "Asia Pacific (Tokyo)",
				"value": "ap-northeast-1"
			},
			{
				"name": "Asia Pacific (Seoul)",
				"value": "ap-northeast-2"
			},
			{
				"name": "Asia Pacific (Singapore)",
				"value": "ap-southeast-1"
			},
			{
				"name": "Asia Pacific (Sydney)",
				"value": "ap-southeast-2"
			},
			{
				"name": "Asia Pacific (Mumbai)",
				"value": "ap-south-1"
			},
			{
				"name": "South America (São Paulo)",
				"value": "sa-east-1"
			},
			{
				"name": "US Gov West 1",
				"value": "us-gov-west-1"
			},
			{
				"name": "US Gov East 1",
				"value": "us-gov-east-1"
			}
		]
	},
	{
		"hidden": true,
		"name": "detail=whcs_storageClass",
		"field": lang['Storage Class'],
		"fieldType": "select",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": [{
				"name": "S3 Standard",
				"value": "STANDARD"
			},
			{
				"name": "S3 Standard-IA",
				"value": "STANDARD_IA"
			},
			{
				"name": "S3 One Zone-IA",
				"value": "ONEZONE_IA"
			},
			{
				"name": "S3 Intelligent-Tiering",
				"value": "INTELLIGENT_TIERING"
			},
			{
				"name": "S3 Glacier",
				"value": "GLACIER"
			},
			{
				"name": "S3 Glacier Deep Archive",
				"value": "DEEP_ARCHIVE"
			}
		]
	},
	{
		"hidden": true,
		"name": "detail=whcs_log",
		"field": lang['Save Links to Database'],
		"fieldType": "select",
		"selector": "h_whcssld",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": [{
				"name": lang.No,
				"value": "0"
			},
			{
				"name": lang.Yes,
				"value": "1"
			}
		]
	},
	{
		"hidden": true,
		"name": "detail=use_whcs_size_limit",
		"field": lang['Use Max Storage Amount'],
		"fieldType": "select",
		"selector": "h_whcszl",
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"form-group-class-pre-layer": "h_whcssld_input h_whcssld_1",
		"description": "",
		"default": "",
		"example": "",
		"possible": [{
				"name": lang.No,
				"value": "0"
			},
			{
				"name": lang.Yes,
				"value": "1"
			}
		]
	},
	{
		"hidden": true,
		"name": "detail=whcs_size_limit",
		"field": lang['Max Storage Amount'],
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"form-group-class-pre-layer": "h_whcssld_input h_whcssld_1",
		"description": "",
		"default": "10000",
		"example": "",
		"possible": ""
	},
	{
		"hidden": true,
		"name": "detail=whcs_dir",
		"field": lang['Save Directory'],
		"form-group-class": "autosave_whcs_input autosave_whcs_1",
		"description": "",
		"default": "/",
		"example": "",
		"possible": ""
	},
  {
    "hidden": true,
    "name": "detail=whcs_delivery_hostname",
    "field": lang['Delivery Hostname'],
    "form-group-class": "autosave_whcs_input autosave_whcs_1",
    "description": "",
    "default": "",
    "example": "",
    "possible": ""
  }
]
    }
}
